# The Plane

## Arrival

Look what came in the mail!

![The package](images/package.jpg)

Oh wait I need to open it for that...

![The package but opened](images/open_package.jpg)

And look who came to visit 😊

I guess we should take a closer look, all that packaging is getting in the way.

![The plane as delivered](images/original_plane.jpg)

Lots of work ahead of us. I hope the tiger will help, he's hiding right now since the plane is so dirty 😉

## Disassembly

![The plane taken apart](images/taken_apart.jpg)

Now that the plane is fully disassembled let's try to find out exactly when it's from. 

The Art of Craftmanship has a great video on [Restoring a Stanley #4 Hand Plane](https://www.youtube.com/watch?v=1xiEzt9EbVc) (the same plane I'm restoring) and they link to a step by step guide on [Wood and Shop](https://woodandshop.com/identify-stanley-hand-plane-age-type-study/) to find the production date of your plane.

Let's follow it:

1. “How Many Patent Dates do you see behind the handplane frog?”

   ![No patents](images/handplane_behind_frog.jpg)
   None.

2. “Do you see a raised ring surrounding the knob receiver screw hole?”

   ![A raised ring surroundind the knob receiver](images/handplane_knob_receiver.jpg)
   Yes.

3. “Is the plane bed painted dark blue?”

   No, it's black.

4. “Do you see a raised rib on toe (front) and heal (rear)?”

   ![No raised ribs](images/handplane_toe_and_heal.jpg)
   No.

**Final results:** Stanley Bailey Type 15 Hand Plane manufactured 1931-1932.

------

A couple other videos I'd like to mention are [Restoring the Bench Plane](https://www.youtube.com/watch?v=RYyV6IUpsYk) by Paul Sellers and [Restoring a Stanley No.4 Hand Plane [32]](https://www.youtube.com/watch?v=7DFpogkKc5A) by JMakes. I used advice from these videos during the entire process of restoration. 

So thank you all.

------

## Cleanup

As you can see from the pictures, it is actually in pretty remarkable shape. So hopefully the next steps won't be too difficult.

We'll start off by soaking in vinegar (after a little touch sanding) to get rid of as much rust as possible.

![The plane swimming in vinegar](images/soaking_in_vinegar.jpg)

24 or so hours later.

![The plane pieces after soaking in vinegar](images/after_vinegar_bath.jpg)

Not as magical as I hoped, but still pretty good progress. Some more sanding and we'll be good to go.